<?require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$product_id = intval($_POST["product_id"]);

if(CModule::IncludeModule("catalog") && $product_id > 0)
{
    echo Add2BasketByProductID($product_id, 1, array(), array());
}
?>