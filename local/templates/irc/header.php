<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<html class="mdl-js">
<head>
    <?
    use Bitrix\Main\Page\Asset;
    ?>
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle('iru');?></title>

    <?
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style_fix.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/slick.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/slick-theme.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.min.css");
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/custom.css");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/fix.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-1.11.0.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-migrate-1.2.1.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slider.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/basket.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/acc.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/stars.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom.js");
    ?>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<main>
	<header>
		<section class="line fix">
            <div>

                    <?
                    CModule::IncludeModule("iblock");
                    $obj = new CIBlockElement;
                    $IBLOCK_ID = "2";

                    $arSelectElement = Array("ID", "NAME", "PREVIEW_TEXT");
                    $arFilterElement = Array("IBLOCK_ID"=> $IBLOCK_ID, "ACTIVE"=>"Y");
                    $resElement = $obj->GetList(Array(), $arFilterElement, false, false, $arSelectElement);
                    $i = 1;
                    while($obElement = $resElement->Fetch()) {?>
                        <p class="adv_text <? if ($i == 1) echo 'active';?>"><? echo $obElement['PREVIEW_TEXT']; ?></p>

                    <? $i++;} ?>

            </div>
			<div>
			  <ul>
				<li><a href="/catalog/?q="><img src="<?=SITE_TEMPLATE_PATH?>/img/search.png"></a></li>
				<li>
                    <?
                    global $USER;
                    if($USER->isAuthorized()){
                        ?> <a href="/personal/"><img src="<?=SITE_TEMPLATE_PATH?>/img/user.png"></a> <?
                    }
                    else {
                        ?> <a href="/login/"><img src="<?=SITE_TEMPLATE_PATH?>/img/user.png"></a> <?
                    }?>
                </li>
				<li><a href="/favorite/"><img src="<?=SITE_TEMPLATE_PATH?>/img/fav.png"></a></li>
				<li><a href="/personal/cart/"><img src="<?=SITE_TEMPLATE_PATH?>/img/basket.png"></a></li>
			  </ul>
			</div>
		</section>
		<section class="logo">
			<a href="/">
				<img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png">
			</a>
		</section>
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top_menu",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(""),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "N"
            )
        );?>
	</header>