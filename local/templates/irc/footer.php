
<?$APPLICATION->IncludeComponent("bitrix:sender.subscribe", "subscribe", Array(
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CONFIRMATION" => "Y",	// Запрашивать подтверждение подписки по email
    "HIDE_MAILINGS" => "N",	// Скрыть список рассылок, и подписывать на все
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "SHOW_HIDDEN" => "N",	// Показать скрытые рассылки для подписки
    "USER_CONSENT" => "N",	// Запрашивать согласие
    "USER_CONSENT_ID" => "0",	// Соглашение
    "USER_CONSENT_IS_CHECKED" => "Y",	// Галка по умолчанию проставлена
    "USER_CONSENT_IS_LOADED" => "N",	// Загружать текст сразу
    "USE_PERSONALIZATION" => "Y",	// Определять подписку текущего пользователя
    "COMPONENT_TEMPLATE" => ".default"
),
    false
);?>
</article>
    <footer>
      <div class="social">
        <div class="s"><img src="<?=SITE_TEMPLATE_PATH?>/img/s_01.png"/><span><a class="contact_link" href="https://www.instagram.com/irc247/?hl=ru">@IRC247</a></span></div>
          <div class="s"><img src="<?=SITE_TEMPLATE_PATH?>/img/s_02.png"/><span><a class="contact_link" href="tel:84956466626">8 (495) 646-66-26</a></span></div>
        <div class="s"><img src="<?=SITE_TEMPLATE_PATH?>/img/s_03.png"/><span><a class="contact_link" href="mailto:info@irc247.ru">info@irc247.ru</a></span></div>
      </div>
      <div class="search">
        <div class="text">НАЙТИ СВОЙ МАГАЗИН</div>
        <form action="">
          <input type="text" placeholder="Укажите город"/>
          <input type="submit" value="ИСКАТЬ"/>
        </form>
      </div>
      <div class="safe">
        <div class="cards">
          <div class="lock"><img src="<?=SITE_TEMPLATE_PATH?>/img/lock.png"/></div>
          <div class="text"><span>БЕЗОПАСНЫЕ ПЛАТЕЖИ</span>
            <ul>
              <li><img src="<?=SITE_TEMPLATE_PATH?>/img/visa.png"/></li>
              <li><img src="<?=SITE_TEMPLATE_PATH?>/img/mastercard.png"/></li>
              <li><img src="<?=SITE_TEMPLATE_PATH?>/img/mir.png"/></li>
            </ul>
          </div>
        </div>
        <div class="text"><span>©IRC ВСЕ МЕЖДУНАРОДНЫЕ ПРАВА ЗАЩИЩЕНЫ</span></div>
      </div>
      <div class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/img/logo_b.png"/></div>
    </footer>
  </main>
</body>