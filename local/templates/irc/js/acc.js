// open all lists

/*$(document).ready(function(){
	$('.lm > li > ul').hide();
	$('.lm > li > a').addClass('active');
	$('.lm > li > a').click(function(){
		$(this).parent('li').children('ul').toggle(function(){
			$(this).parent('li').children('a').toggleClass('active');
		});
	});
}); */


// open one list & close others

$(document).ready(function(){
	$('.lm > li > ul').hide();
	$('.lm > li > a').click(function(){
		$(this).parent('li').children('ul').toggle();
		$(this).parent('li').siblings('li').children('ul').hide();
		$(this).parent('li').children('a').toggleClass('active');
		$(this).parent('li').siblings('li').children('a').removeClass('active');
	});
}); 