
$(document).ready(function(){
    $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1
    });
    $('.slider_').slick({
        slidesToShow: 4,
        slidesToScroll: 1
    });
    $('.item_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.item_slide'
    });
    $('.item_slide').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.item_slider',
        focusOnSelect: true
    });
});