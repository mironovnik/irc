<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);


?>

<article>
    <section class="item_main fix-item">
        <div class="bread">
            <p>
                <a href="#">Магазин</a>
                <span>/</span>
                <a href="#">Лицо</a>
                <span>/</span>
                <a href="#">Средства для очищения</a>
                <span>/</span>
                <a href="#">Мягкий гипоаллергенный крем для очищения SOFT TOUCH</a>
            </p>
        </div>

        <div class="slider_items">
            <div class="item_slider">
                <?foreach($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto) {
                    $file = new CFile;
                    $file_path = $file->GetPath($arPhoto);
                    ?><div class="img"> <img src="<?=$file_path?>"></div> <?
                }?>
            </div>
            <div class="item_slide">
                <?foreach($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto) {
                    $file = new CFile;
                    $file_path = $file->GetPath($arPhoto);
                    ?><div class="img"> <img src="<?=$file_path?>"></div> <?
                }?>
            </div>
        </div>

        <div class="item_info">
            <p><?=$arResult['PREVIEW_TEXT'];?></p>
            <div class="about">
                <section class="menu_l">
                    <ul class="lm">
                        <li><a class="head">О СРЕДСТВЕ</a>
                            <ul>
                                <li><?=$arResult['PROPERTIES']['ABOUT_PRODUCT']['VALUE'];?></li>
                            </ul>
                        </li>
                        <li><a class="head">ДЛЯ КОГО?</a>
                            <ul>
                                <li><?=$arResult['PROPERTIES']['FOR_WHO']['VALUE'];?></li>
                            </ul>
                        </li>
                        <li><a class="head">АКТИВНЫЕ КОМПОНЕНТЫ</a>
                            <ul>
                                <li><?=$arResult['PROPERTIES']['ACTIVE_COMPONENTS']['VALUE'];?></li>
                            </ul>
                        </li>
                        <li><a class="head">СПОСОБ ПРИМЕНЕНИЯ</a>
                            <ul>
                                <li><?=$arResult['PROPERTIES']['WAY_TO_USE']['VALUE'];?></li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </div>
            <div class="cost">
                <div class="price">
                    <span><?=$arResult['PRICES']['BASE']['VALUE'].' '.$arResult['PRICES']['BASE']['CURRENCY']?></span>
                </div>
                <div class="stars">
                    <ul>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul><span><a href="#">3 отзыва</a> | <a href="#">Написать отзыв</a></span>
                </div>
            </div>
            <button data-id="<?=$arResult['ID']?>" class="btn product-to-cart">ДОБАВИТЬ В КОРЗИНУ</button>
        </div>
    </section>
    <?
    $obj = new CIBlockElement;
    $objImage = new CFile;
    $objPrice = new CPrice;
    if (!empty($arResult['PROPERTIES']['BUY_WITH']['VALUE'])) {
    ?>
    <section class="hot item fix">
        <h2>С ЭТИМ ПРОДУКТОМ ПОКУПАЮТ</h2>
        <div class="slider">

            <? foreach ($arResult['PROPERTIES']['BUY_WITH']['VALUE'] as $arItem) {
                ?><div class="el"> <?
                $res = $obj->GetList(array("SORT" => "ASC"), array('IBLOCK_ID' => 1, 'ID' => $arItem), false, false, array('ID','PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'));
                while ($ob = $res->GetNextElement()) {
                    $arProduct = $ob->GetFields();
                    $Image = $objImage->GetPath($arProduct['PREVIEW_PICTURE']);
                    $price = $objPrice->GetBasePrice($arProduct['ID']);
                }?>
                <img src="<?=$Image;?>">
                <span class="name"><?=$arProduct['PREVIEW_TEXT'];?></span>
                <span class="price"><?=$price['PRICE'].' '.$price['CURRENCY'];?></span>
                <a class="btn" href="<?=$arProduct['DETAIL_PAGE_URL'];?>">Купить</a>
                </div>
            <?}?>

        </div>
    </section>
    <? } ?>
    <section class="mail fix"><span>ПОДПИШИСЬ НА НОВОСТИ И ПОЛУЧИ АЛЬГИНАТНУЮ МАСКУ В ПОДАРОК</span>
        <form action="">
            <input type="mail" placeholder="Ваш E-mail"/>
            <input type="submit" value="ПОДПИСАТЬСЯ"/>
        </form>
    </section>
</article>
