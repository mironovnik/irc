<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

?>

        <section class="hot fix">
            <h2>ХИТЫ ПРОДАЖ</h2>
            <div class="slider">

            <?foreach($arResult["ITEMS"] as $arItem ):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="el" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><img alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></a>
                    <a class="product_link" href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><span class="name"><?=$arItem['PREVIEW_TEXT']?></span></a>
                    <span class="price"><?=$arItem['PRICES']['BASE']['VALUE'].' '.$arItem['PRICES']['BASE']['CURRENCY']?></span>
                    <a data-id="<?=$arItem['ID']?>" class="btn product-to-cart" href="#">КУПИТЬ</a>
                </div>
            <?endforeach;?>
            </div>
        </section>
