<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(false);


?>
<article>
    <section class="shop fix-shop">
        <h2>НОВИНКИ | БЕСТСЕЛЛЕРЫ</h2>
        <section class="menu_l">
            <ul class="lm">
                <li><a class="head">Лицо</a>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog_menu", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "left-1",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </li>
                <li><a class="head">Волосы</a>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog_menu", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "left-1",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </li>
                <li><a class="head">Тело</a>
                    <?$APPLICATION->IncludeComponent("bitrix:menu", "catalog_menu", Array(
                        "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                        "CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
                        "DELAY" => "N",	// Откладывать выполнение шаблона меню
                        "MAX_LEVEL" => "1",	// Уровень вложенности меню
                        "MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
                            0 => "",
                        ),
                        "MENU_CACHE_TIME" => "360000",	// Время кеширования (сек.)
                        "MENU_CACHE_TYPE" => "Y",	// Тип кеширования
                        "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                        "ROOT_MENU_TYPE" => "left-1",	// Тип меню для первого уровня
                        "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    ),
                        false
                    );?>
                </li>
            </ul>
        </section>
        <section class="items" >
            <?foreach($arResult["ITEMS"] as $arItem ):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>


                <div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><img alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>"></a>
                    <a class="product_link" href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank"><span class="name"><?=$arItem['PREVIEW_TEXT']?></span></a>
                    <span class="price"><?=$arItem['PRICES']['BASE']['VALUE'].' '.$arItem['PRICES']['BASE']['CURRENCY']?></span>
                    <a data-id="<?=$arItem['ID']?>" class="btn product-to-cart" href="#">КУПИТЬ</a>
                </div>

            <?endforeach;?>
        </section>
    </section>
</article>
